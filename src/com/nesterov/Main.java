package com.nesterov;

public class Main {
    public static void main(String[] args) {
        String[][] board1 = {{"X", "0", "X"}, {"0", "X", "0"}, {"_", "_", "X"}};
        String[][] board2 = {{"X", "X", "0"}, {"_", "0", "_"}, {"0", "X", "0"}};
        String[][] board3 = {{"_", "0", "X"}, {"X", "_", "_"}, {"_", "_", "0"}};
        String[][] board4 = {{"X", "0", "X"}, {"0", "X", "X"}, {"0", "X", "0"}};

        System.out.println(solution(board1));
        System.out.println(solution(board2));
        System.out.println(solution(board3));
        System.out.println(solution(board4));
    }

    public static String solution(String[][] board) {
        // Проверка строк и столбцов на выигрышные комбинации для игроков 'X' и 'O'.
        for (int i = 0; i < 3; i++) {
            // Проверка строк
            if (board[i][0].equals(board[i][1]) && board[i][0].equals(board[i][2])) {
                if (!board[i][0].equals("_")) {
                    return board[i][0] + " won";
                }
            }
            // Проверка столбцов.
            if (board[0][i].equals(board[1][i]) && board[0][i].equals(board[2][i])) {
                if (!board[0][i].equals("_")) {
                    return board[0][i] + " won";
                }
            }
        }

        // Проверка диагоналей на выигрышные комбинации.
        if ((board[0][0].equals(board[1][1]) && board[0][0].equals(board[2][2])) ||
                (board[0][2].equals(board[1][1]) && board[0][2].equals(board[2][0]))) {
            if (!board[1][1].equals("_")) { // Общая ячейка диагонали, достаточно проверки для любой диагонали.
                return board[1][1] + " won";
            }
        }

        // Проверка наличия незаполненных ячеек.
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j].equals("_")) {
                    return "Next";
                }
            }
        }

        // Если все ячейки заполнены и нет выигрышной комбинации.
        return "Game over";
    }
}